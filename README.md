# DBI Examples

This repository contains several examples from the introductory database
course DBI at WHS, Campus Bocholt (Westfälische Hochschule, University of Applied Sciences):

    CAP_DB          - SQL script to create database schema and
                      initialize database  
    CustomerOveriew - a simple JDBC example for PostgreSQL  

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0) SWTLab WHS